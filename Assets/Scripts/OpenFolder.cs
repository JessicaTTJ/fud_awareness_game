﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class instantiates and makes a gameobject visible on mouse-click
 * 
 * */
public class OpenFolder : MonoBehaviour
{
    public GameObject targetFile;
    int mouseclick = 0;

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mouseclick++;
            if (mouseclick == 1)
            {
                Instantiate(targetFile);
                targetFile.SetActive(true);
            }
        }
    }
}
