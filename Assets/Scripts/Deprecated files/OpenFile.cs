﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the double-click to open a file. 
 * It's not used anymore since all clickable files/urls/attachments use the OpenPopUp.cs script.
 * Functionalities in this class are obsolete but will remain in the Deprecated Files folder for reference purposes.
 * 
 * */
public class OpenFile : MonoBehaviour
{
    public GameObject targetFile;
    bool exist = false;
    int mouseclick = 0;

    private void Start()
    {
        targetFile.SetActive(false);
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if (Input.GetMouseButtonDown(0))
    //    {
    //        Instantiate(targetFile);
    //        targetFile.SetActive(true);
    //        exist = true;
    //    }
    //    if (exist)
    //    {
    //        exist = false;
    //    }
    //}

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
                mouseclick++;
            if (mouseclick == 2)
            {
                Instantiate(targetFile);
                targetFile.SetActive(true);
            }
        }
    }
}
