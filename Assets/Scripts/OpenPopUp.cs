﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class makes a gameobject visible/active on mouse-click.
 * 
 * */
public class OpenPopUp : MonoBehaviour
{
    public GameObject targetFile;

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
                targetFile.SetActive(true);
        }
    }
}
