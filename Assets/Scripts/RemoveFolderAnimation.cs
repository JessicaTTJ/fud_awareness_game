﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class makes the animation folder (with orange background) invisible
 * 
 * */

public class RemoveFolderAnimation : MonoBehaviour
{
    public GameObject orangeFolder;

    private void OnMouseDown()
    {
        orangeFolder.SetActive(false);
    }
}
