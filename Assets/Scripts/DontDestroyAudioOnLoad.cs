﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  Don't destroy the audioHandler (background music) when reloading the scene and make sure there is 
 *  only 1 audio handler currently active, otherwise the background music will overlap.
 *  
 * */
public class DontDestroyAudioOnLoad : MonoBehaviour
{
    public GameObject[] audioHandler;

    private void Awake()
    {      
        audioHandler = GameObject.FindGameObjectsWithTag("music"); // don't forget to assign the tag!

        if (audioHandler.Length > 1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
}
