﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * This class handles the logic for the feedback pop-up after the player clicks on a fraud/no fraud button.
 * If the player picks the wrong choice, a live gets deducted.
 * When the player's game-over, the game will restart after 6 seconds
 * 
 * */
public class FeedbackToUser : MonoBehaviour
{
    public GameObject correctAnswer;
    public GameObject wrongAnswer;

    public bool rightAnswer;

    public static int health = 2;

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0) && rightAnswer)
        {
            correctAnswer.SetActive(true);
        }

        if (Input.GetMouseButtonDown(0) && !rightAnswer)
        {
            wrongAnswer.SetActive(true);
            health--;

            if(health <= 0)
            {
                Debug.Log("Game over! your current health is: " + health);
                // show game-over, restart scene/level after x seconds (coroutine)
                StartCoroutine("RestartScene");
            }
        }
    }
    // Reload scene and reset the health and mailAmount
    IEnumerator RestartScene()
    {
        yield return new WaitForSeconds(6f);
        Debug.Log("Reloading the scene");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        health = 2;
        RemoveMail.mailAmount = 9;
    }
}
