﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * This class handles the 'lives left' UI text, gameOver pop-up and keeps track of the mailAmount
 * 
 * */
public class Health : MonoBehaviour
{
    public Text healthText;
    public Text mailAmountText;
    public GameObject gameOver;

    public AudioClip clap;
    AudioSource audioSource;
    bool hasPlayed = false;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        healthText = GameObject.Find("HealthText").GetComponent<Text>();
        mailAmountText.enabled = false;
    }

    void Update()
    {
        healthText.text = "Lives left: " + FeedbackToUser.health;
        if(FeedbackToUser.health <= 0)
        {
            gameOver.SetActive(true);
        }

        // show UI text and play a sound effect
        if(RemoveMail.mailAmount <= 0)
        {
            mailAmountText.enabled = true;
            mailAmountText.text = "Gefeliciteerd! je hebt alle fraude mails ontdekt!";
            if (!hasPlayed)
            {
                audioSource.PlayOneShot(clap, 1f);
                hasPlayed = true;
            }
        }
    }

}
