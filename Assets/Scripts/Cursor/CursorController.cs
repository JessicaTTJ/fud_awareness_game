﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class controls the cursor behaviour when hovering over attachments and urls.
 * A separate GameObject has been created that has this script attached. Do not attach this script to new gameobjects, 
 * use the 'ChangeCursor.cs'script. 
 * When creating the cursor sprite, do not forget to change the 'Texture Type' in the sprite settings to 'Cursor'
 * */
public class CursorController : MonoBehaviour
{
    [SerializeField]
    public Texture2D hoverCursor;

    public static CursorController Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hitInfo;
        if(Physics.Raycast(ray, out hitInfo, Mathf.Infinity))
        {
            if(hitInfo.collider.GetComponent<ChangeCursor>()!= null)
            {
                SetHoverCursor();
            }
            else
            {
                SetDefaultCursor();
            }
        }
    }

    public void SetHoverCursor() // change to hand cursor when hovering over attachments and urls
    {
        Cursor.SetCursor(hoverCursor, Vector2.zero, CursorMode.Auto);
    }

    public void SetDefaultCursor()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }
}
