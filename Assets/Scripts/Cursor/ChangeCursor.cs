﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class changes the cursor when hovering over attachments and urls in mails.
 * You may attach these to your newly created gameobjects.
 * */

public class ChangeCursor : MonoBehaviour
{
    private void OnMouseEnter()
    {
        CursorController.Instance.SetHoverCursor();
    }

    private void OnMouseExit()
    {
        CursorController.Instance.SetDefaultCursor();
    }
}
