﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class makes a gameobject inactive/invisible on mouse-click.
 * 
 * */
public class CloseFolder : MonoBehaviour
{
    public GameObject targetFile;

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
                targetFile.SetActive(false);          
        }
    }
}
