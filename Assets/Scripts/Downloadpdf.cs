﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class makes the folder icon blink when downloading the PDF from John Doe's mail.
 * 
 * */

public class Downloadpdf : MonoBehaviour
{
    public GameObject orangeFolder;

    private void OnMouseDown()
    {
        StartCoroutine("BlinkFolder");
    }

    IEnumerator BlinkFolder()
    {
        Debug.Log("starting the blinking animation");
        orangeFolder.SetActive(false);
        yield return new WaitForSeconds(.5f);
        orangeFolder.SetActive(true);

        yield return new WaitForSeconds(.5f);
        orangeFolder.SetActive(false);

        yield return new WaitForSeconds(.5f);
        orangeFolder.SetActive(true);
    }
}
