﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
 * This class handles the timer and reloads the scene after 6 seconds, amount of lives and mailAmount when the timer hits 0
 * 
 * */
public class CountDownTimer : MonoBehaviour
{
    public float currentTime = 0f;
    public float startingTime = 10f;
    [SerializeField] Text cdText;

    public GameObject gameOverpopUp;
    public GameObject startButton;

    public bool startTimer;

    void Start()
    {
        currentTime = startingTime;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startTimer = true;
        }
        if (startTimer)
        {
            startButton.SetActive(false);

            currentTime -= 1 * Time.deltaTime;
            cdText.text = "Time left: " + currentTime.ToString("0");

            // change the text color when the countdown reaches <10
            if (currentTime <= 10)
            {
                cdText.color = Color.red;
            }
            if (currentTime <= 0)
            {
                gameOverpopUp.SetActive(true);
                // currentTime = 0;
                StartCoroutine("RestartScene");

            }
            if(RemoveMail.mailAmount <= 0) // stop timer when all mails have been successfully cleared
            {
                startTimer = false;
            }
        }
    }

    IEnumerator RestartScene()
    {
        yield return new WaitForSeconds(6f);
        Debug.Log("Reloading the scene");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        FeedbackToUser.health = 2;
        RemoveMail.mailAmount = 9;
    }
}
