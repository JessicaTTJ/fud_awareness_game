﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class removes the mail and mail thumbnail when the user closes a feedback pop-up.
 * The mailAmount is needed to show the 'Congratulations' UI text at the end when all
 * mails have been successfully cleared.
 * 
 * */
public class RemoveMail : MonoBehaviour
{
    public GameObject mailThumbnail;
    public GameObject mailContent;

    public static int mailAmount = 9;

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Destroy(mailThumbnail);
            Destroy(mailContent);
            mailAmount--;
            Debug.Log("Mail amount is " + mailAmount);
        }
    }
}
