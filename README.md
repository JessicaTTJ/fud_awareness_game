# [NL] Cyber Security Awareness Game

Deze awareness game is ontwikkelt voor eerstejaarsstudenten om bewustzijn te creëren over online veiligheid. 

Dit is het eerste level dat gaat over het doorzoeken van mailberichten op fraude. 

Het tweede level gaat over sociale media en bestaat uit een mock-up dat in je browser te spelen is: https://marvelapp.com/5d98aeh/screen/64199750 

De controls zijn: je linkermuisknop :) 

De game is gemaakt met Unity (versie 2019.2.3f1). 

# Benodigdheden
* Unity Game Engine (bij voorkeur versie 2019, andere versies zijn niet getest)
* Om de game aan te passen heb je basiskennis nodig van Unity en C#

# Download
Je kunt de repository clonen door dit te kopiëren (in een Git client bijvoorbeeld): **git clone git@bitbucket.org:JessicaTTJ/fud_awareness_game.git**

# Bugs die bekend zijn
* Wanneer je over een link zweeft met je muis verandert het in een handje, maar soms verandert deze niet terug naar de normale muiswijzer. Je kunt dit verhelpen door nogmaals over een willekeurige link te zweven en de muis wordt weer zoals het was.

* Het kan gebeuren wanneer je op de fraude of geen fraude knop klikt en feedback krijgt, dat er geen kruisje onderaan de pop-up verschijnt om deze te sluiten. In dit geval kun je onderaan in het midden van de pop-up klikken om het te sluiten.


# Copyright & Credits

![Alt text](https://licensebuttons.net/l/by/3.0/88x31.png)

Tenzij anders vermeld is alles in dit werk gelicenseerd onder een Creative Commons Naamsvermelding 4.0-licentie. 
Wanneer je gebruik wilt maken van dit werk, hanteer dan de volgende methode van naamsvermelding:
Jessica Nguyen, FUD Awareness Game (2019), CC-BY 4.0 gelicenseerd. De volledige licentie-tekst is te lezen op: 
https://creativecommons.org/licenses/by/4.0/.

Het spel refereert naar merknamen zonder toestemming gevraagd te hebben aan deze bedrijven. De merknamen zijn niet geassocieerd, geautoriseerd of gesponsord door de bedrijven zelf.  

Gebruikte muziek:

* Achtergrondmuziek: https://www.youtube.com/watch?v=xy_NKN75Jhw 

* SFX: https://freesound.org/people/fisch12345/sounds/325113/ 

* SFX: https://freesound.org/people/Robinhood76/sounds/268693/ 

